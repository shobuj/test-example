<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Google_Client;
use Google_Service_Directory;

class GoogleCalendarController extends Controller
{
    public function getClient()
	{
    	$client = new Google_Client();
        $client->setApplicationName(config('app.name'));
        //$client->setScopes(Google_Service_Directory::CALENDAR_READONLY);
        $client->setAuthConfig(storage_path('client_secret.json'));
        $client->setAccessType('offline');
        return $client;
	}

}
